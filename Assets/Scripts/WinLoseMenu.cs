﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinLoseMenu : MonoBehaviour
{
    public enum gameState { active, won, lost };
    public gameState currentGameState;
    public GameObject winMenuUI;
    public GameObject loseMenuUI;

    // Start is called before the first frame update
    void Start()
    {
        currentGameState = gameState.active;
    }
    // Update is called once per frame
    void Update()
    {
        switch(currentGameState)
        {
            case gameState.active:
            winMenuUI.SetActive(false);
            loseMenuUI.SetActive(false);
            break;

            case gameState.won:
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            winMenuUI.SetActive(true);
            Time.timeScale = 0f;
            break;

            case gameState.lost:
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            loseMenuUI.SetActive(true);
            Time.timeScale = 0f;
            break;
        }
    }
}

