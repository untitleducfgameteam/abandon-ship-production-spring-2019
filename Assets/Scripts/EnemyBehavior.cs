﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehavior : MonoBehaviour
{
    //Initialize Variables

    //Initialize Patrol Path System
    public GameObject patrollingTarget;
    public GameObject gameUI;
    public Transform enemyPosition;
    public float discoveryDistance = 4f;
    public float trackingDistance = 8f;
    public float fieldOfView = 30f;
    public float raycastCrouchHeight = 1f;
    public float raycastStandingHeight = 1.62f;
    public List<Transform> Waypoints = new List<Transform>();
    private bool patrolpathSafe;
    private bool isInFov = false;
    private float waypointsSafe = 0f;
    private float targetDistance = 0f;
    private int currentWaypoint = 0;
    private Vector3 raycastPosition;
    private Vector3 raycast2Position;
    NavMeshAgent _navMeshAgent;
    Animator _anim;

    //Initialize State Machine
    public enum stateMachine{idolState, patrolState, chaseState };
    public stateMachine enemyState;

    //Render Gizmos for debugging purposes
    public virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, discoveryDistance);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, trackingDistance);

        Vector3 fovLine1 = Quaternion.AngleAxis(fieldOfView, transform.up) * transform.forward * trackingDistance;
        Vector3 fovLine2 = Quaternion.AngleAxis(-fieldOfView, transform.up) * transform.forward * trackingDistance;

        if (isInFov)
        {
            Gizmos.color = Color.green;
        }
        else
        {
            Gizmos.color = Color.blue;
        }
        Gizmos.DrawRay(raycastPosition, fovLine1);
        Gizmos.DrawRay(raycastPosition, fovLine2);

        Gizmos.DrawRay(raycast2Position, fovLine1);
        Gizmos.DrawRay(raycast2Position, fovLine2);

    }
    // Start is called before the first frame update
    void Start()
    {
    
    _navMeshAgent = this.GetComponent<NavMeshAgent>();
    _anim = this.GetComponent<Animator>();
    
    //Set Enemy State
    if (Waypoints.Count == 0)
        {
        enemyState = stateMachine.idolState;
        _anim.SetInteger("EnemyAnimationState", 0);
        print("Enemy State set to idle due to Waypoint Count being 0");
        }
    else
        {
            for (int i = 0; i < Waypoints.Count; i++)
                {
                if (Waypoints[i] == null)
                    {
                    print("Waypoint " + i + " is missing. This must be added in order to access patrol state");
                    }
                else
                    {
                    waypointsSafe++;
                    print("Waypoint " + i + " has been properly initialzed");

                    if (i == (Waypoints.Count - 1))
                        {
                        if (waypointsSafe == Waypoints.Count)
                            {
                            patrolpathSafe = true;
                            }
                        else
                            {
                            patrolpathSafe = false;
                            }
                        }
                    }
                }

            if (patrolpathSafe)
                {
                enemyState = stateMachine.patrolState;
                _anim.SetInteger("EnemyAnimationState", 1);
                print("Enemy State set to partol due to Waypoints being initialized correctly");
                Vector3 targetVector = Waypoints[currentWaypoint].transform.position;
                _navMeshAgent.SetDestination(targetVector);
                print("Enemy going to Waypoint " + currentWaypoint);
            }
            else
                {
                enemyState = stateMachine.idolState;
                _anim.SetInteger("EnemyAnimationState", 0);
                print("Enemy State set to idle due to " + (Waypoints.Count - waypointsSafe) + " uninitialized waypoint(s)");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        targetDistance = Vector3.Distance(transform.position, patrollingTarget.transform.position);
        raycastPosition = transform.position;
        raycast2Position = transform.position;
        raycastPosition.y += raycastCrouchHeight;
        raycast2Position.y += raycastStandingHeight;
        isInFov = inFOV(transform, enemyPosition, fieldOfView, trackingDistance, raycastCrouchHeight, raycastStandingHeight);
        if (isInFov)
        {
            print("Enemy currently sees player");
        }
        if (targetDistance < 1.5f)
        {
            gameUI.GetComponent<WinLoseMenu>().currentGameState = WinLoseMenu.gameState.lost;
        }
        switch (enemyState)
            {
            case stateMachine.idolState:
            IdolBehavior();
            break;

            case stateMachine.patrolState:
            PatrolBehavior();
            break;

            case stateMachine.chaseState:
            ChaseBehavior();
            break;
        }
    }

    public static bool inFOV(Transform checkingObject, Transform target, float maxAngle, float maxRadius, float raycastingCHeight, float raycastingSHeight)
    {
        if (Vector3.Distance(checkingObject.position, target.position) < maxRadius)
          {
            Vector3 directionBetween = (target.position - checkingObject.position).normalized;
            directionBetween.y *= 0;

            float angle = Vector3.Angle(checkingObject.forward, directionBetween);
            Vector3 checkingObjectPosition = checkingObject.position;
            Vector3 targetPosition = target.position;
            checkingObjectPosition.y += raycastingCHeight;
            targetPosition.y += raycastingCHeight;
            bool standingRaycast = false;
            bool crouchingRaycast = false;
            if (angle <= maxAngle)
            {
                
                Ray ray = new Ray(checkingObjectPosition, targetPosition - checkingObjectPosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, maxRadius))
                {

                    if (hit.transform == target)
                    {
                        crouchingRaycast = true;
                    }

                }

                checkingObjectPosition = checkingObject.position;
                targetPosition = target.position;
                checkingObjectPosition.y += raycastingSHeight;
                targetPosition.y += raycastingSHeight;

                Ray ray2 = new Ray(checkingObjectPosition, targetPosition - checkingObjectPosition);
                RaycastHit hit2;

                if (Physics.Raycast(ray2, out hit2, maxRadius))
                {

                    if (hit2.transform == target)
                    {
                        standingRaycast = true;
                    }

                }

            }  

            if (standingRaycast && crouchingRaycast)
            {
                return true;
            }

            if (standingRaycast && !crouchingRaycast)
            {
                return true;
            }
        }
        return false;
    }

    void IdolBehavior()
    {
    //Stop Movement
    _navMeshAgent.enabled = false;

    //Condition for switching to Chase State
    if (isInFov)
        {
        _navMeshAgent.enabled = true;
        _anim.SetInteger("EnemyAnimationState", 2);
        Vector3 targetVector = patrollingTarget.transform.position;
        _navMeshAgent.SetDestination(targetVector);
        enemyState = stateMachine.chaseState;
        print("Enemy switched into Chase State");
        }
    }

    void PatrolBehavior()
    {
     //Cycle through Waypoints
    if (_navMeshAgent.remainingDistance <= 1.0f)
        {
        if (currentWaypoint == (Waypoints.Count - 1))
            {
            currentWaypoint = 0;
            }
        else
            {
            currentWaypoint++;
            }

        Vector3 targetVector = Waypoints[currentWaypoint].transform.position;
        _navMeshAgent.SetDestination(targetVector);
        print("Enemy going to Waypoint " + currentWaypoint);
        }

    //Condition for switching to Chase State
    if (isInFov)
        {
        Vector3 targetVector = patrollingTarget.transform.position;
        _navMeshAgent.SetDestination(targetVector);
        enemyState = stateMachine.chaseState;
        _anim.SetInteger("EnemyAnimationState", 2);
        print("Enemy switched into Chase State");
        }
    }

    void ChaseBehavior()
    {
    //Update Player Position
    Vector3 targetVector = patrollingTarget.transform.position;
    _navMeshAgent.SetDestination(targetVector);

     //Condition for switching to Patrol State
    if (targetDistance > trackingDistance)
        {
        enemyState = stateMachine.patrolState;
        _anim.SetInteger("EnemyAnimationState", 1);
        print("Enemy switched into Patrol State");
        }
    }
}
