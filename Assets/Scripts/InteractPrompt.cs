﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractPrompt : MonoBehaviour
{
    public float viewDistance = 3f;
    public GameObject Player;
    private float targetDistance = 1f;
    private bool canOpen = false;
    public string interactTextKeyboard;
    public string interactTextGamepad;
    public float TextX = 960f;
    public float TextY = 900f;
    public Font font;


    public virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, viewDistance);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
        {
            targetDistance = Vector3.Distance(transform.position, Player.transform.position);

            //Get Distance between Player and Keypad
            if (targetDistance <= viewDistance)
            {
                canOpen = true;
            }
            else
            {
                canOpen = false;
            }
        }

    void OnGUI()
    {
        if (canOpen)
        {
            GUI.skin.font = font;
            if (Input.GetJoystickNames().Length > 0)
            {
                GUI.Label(new Rect(TextX, TextY, 500, 20), interactTextGamepad);
            }
            else
            {
                GUI.Label(new Rect(TextX, TextY, 500, 20), interactTextKeyboard);
            }
        }
    }
}
