﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keypadEscape : MonoBehaviour
{

    public GameObject Player;
    public GameObject gameUI;
    public float viewDistance = 3f;
    private float targetDistance = 1f;
    private bool canOpen = false;


    public virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, viewDistance);
    }

    // Start is called before the first frame update
    void Start()
    {

    }




    // Update is called once per frame
    void Update()
    {
        targetDistance = Vector3.Distance(transform.position, Player.transform.position);

        //Get Distance between Player and Keypad
        if (targetDistance <= viewDistance)
        {
            canOpen = true;
        }
        else
        {
            canOpen = false;
        }

        if (canOpen)
        {
            print("You are by the keypad");
        }
        if ((Input.GetButtonDown("Interact")) && canOpen)
        {
            gameUI.GetComponent<WinLoseMenu>().currentGameState = WinLoseMenu.gameState.won;
            print("Door is the opened");
        }
    }
}