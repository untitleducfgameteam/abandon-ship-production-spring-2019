﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fpsController : MonoBehaviour {

    //Initialize Variables
    public float playerSpeed = 7f; //Player Speed
    public float jumpVelocity = 5f; //Player Jump Velocity
    public float vertVelocity = 0f;
    public float cameraSensitivity = 0.15f; //Camera Sensitivity
    public bool invertedCameraHorizontal = false;
    public bool invertedCameraVertical = false;
    public enum inputDevice {autoDetect, KeyboardAndMouse, Gamepad };
    public inputDevice device_input;
    private bool hasJumped;
    private bool isCrouched;
    Animator _anim;
    float hor_movement_input; //Horizontal Movement Input
	float ver_movement_input; //Vertical Movement Input

    float rotation_x_input; //Camera X Rotation Input
    float rotation_y_input; //Camera Y Rotation Input

    float rotation_x; //Camera X Rotation
    float rotation_y; //Camera Y Rotation

	float hor_movement; //Horizontal Movement
	float ver_movement; //Vertical Movement

    private bool jump_input = false;
    private bool crouch_input = false;


    //Intialize Objects
    CharacterController player;
    public GameObject player_camera;
    public GameObject UserInterfaceObject;

    // Use this for initialization
    void Start () {
        //Get Required Components
        player = GetComponent<CharacterController>();
        _anim = this.GetComponent<Animator>();
        device_input = inputDevice.autoDetect;
	}

	// Update is called once per frame
	void Update () {

        //Get Input
        get_input();

        //Jumping
        if (jump_input)
        {
            jump();
        }

        if (crouch_input)
        {
            if (isCrouched == false)
                {
                player.height = player.height / 2;
                playerSpeed = playerSpeed / 2;
                isCrouched = true;
                }

            else
                {
                player.height = player.height * 2;
                playerSpeed = playerSpeed * 2;
                isCrouched = false;
                }

            print("Player Crouched");
            
        }
        //Calculate Camera Direction
        switch(invertedCameraHorizontal)
            {
            case true:
                rotation_x = -rotation_x_input * cameraSensitivity;
                break;

            case false:
                rotation_x = rotation_x_input * cameraSensitivity;
                break;
            }

        switch (invertedCameraVertical)
        {
            case true:
                rotation_y = -rotation_y_input * cameraSensitivity;
                break;

            case false:
                rotation_y = rotation_y_input * cameraSensitivity;
                break;
        }

        rotation_y = Mathf.Clamp(rotation_y, -45, 45);

        //Calculate Movement In-Game Movement Speed
        hor_movement = hor_movement_input * playerSpeed;
	    ver_movement = ver_movement_input * playerSpeed;
        
        ApplyGravity();
        Vector3 movement = new Vector3(hor_movement, vertVelocity, ver_movement);
        

        //Apply Movement & Camera Calculations
        if (!UserInterfaceObject.GetComponent<PauseMenu>().GameIsPaused)
        {
            transform.Rotate(0, rotation_x, 0);
            player_camera.transform.Rotate(-rotation_y, 0, 0);
        }
        movement = transform.rotation * movement;
        player.Move(movement * Time.deltaTime);

        if ((hor_movement == 0) && (ver_movement == 0))
        {
        _anim.SetInteger("PlayerAnimationState", 0);
        }
        else
        {
        _anim.SetInteger("PlayerAnimationState", 1);
        }

        if (!UserInterfaceObject.GetComponent<PauseMenu>().GameIsPaused)
        {
            transform.Rotate(0, rotation_x, 0);
            player_camera.transform.Rotate(-rotation_y, 0, 0);
        }
        print(player_camera.transform.rotation.y);
        //player_camera.transform.rotation.y = Mathf.Clamp(player_camera.transform.rotation.y,0.45f,0.6f);
    }

    void get_input()
    {
        
        //Get Input

        //If you Override the Input Device with the Inspector, this switch statement will pull the correct input
        if (device_input != inputDevice.autoDetect)
        {
        switch (device_input)
            {
                //Override to use Keyboard and Mouse
                case inputDevice.KeyboardAndMouse:
                    rotation_x_input = Input.GetAxis("Horizontal Mouse Axis"); //Mouse Horizontal Axis
                    rotation_y_input = Input.GetAxis("Vertical Mouse Axis"); //Mouse Vertical Axis

                    hor_movement_input = Input.GetAxis("Horizontal Keyboard Axis");//Horizontal Movement Input
                    ver_movement_input = Input.GetAxis("Vertical Keyboard Axis"); //Vertical Movement Input

                    jump_input = Input.GetButtonDown("Jump Button Keyboard"); //Get Keyboard Jump Input
                    crouch_input = Input.GetButtonDown("Crouch Button Keyboard"); //Get Keyboard Crouch Input
                    break;

                //Override to use Gamepad
                case inputDevice.Gamepad:
                    rotation_x_input = Input.GetAxis("Right Joystick Horizontal Axis") * (cameraSensitivity * 75); //Mouse Horizontal Axis
                    rotation_y_input = -Input.GetAxis("Right Joystick Vertical Axis") * (cameraSensitivity * 75); //Mouse Vertical Axis

                    hor_movement_input = Input.GetAxis("Left Joystick Horizontal Axis");//Horizontal Movement Input
                    ver_movement_input = -Input.GetAxis("Left Joystick Vertical Axis"); //Vertical Movement Input

                    jump_input = Input.GetButtonDown("Jump Button Joystick"); //Get Gamepad Jump Input
                    crouch_input = Input.GetButtonDown("Crouch Button Joystick"); //Get Gamepad Crouch Input
                    break;
            }
            
        }
        //If the Device type isn't overridden with inspector, auto assign the input device based off of gamepad avalibility
        else
        {
            //Switch to Gamepad Input
            if (Input.GetJoystickNames().Length > 0)
            {
                rotation_x_input = Input.GetAxis("Right Joystick Horizontal Axis") * (cameraSensitivity * 75); //Mouse Horizontal Axis
                rotation_y_input = -Input.GetAxis("Right Joystick Vertical Axis") * (cameraSensitivity * 75); //Mouse Vertical Axis

                hor_movement_input = Input.GetAxis("Left Joystick Horizontal Axis");//Horizontal Movement Input
                ver_movement_input = -Input.GetAxis("Left Joystick Vertical Axis"); //Vertical Movement Input

                jump_input = Input.GetButtonDown("Jump Button Joystick"); //Get Gamepad Jump Input
                crouch_input = Input.GetButtonDown("Crouch Button Joystick"); //Get Gamepad Crouch Input
            }

            else
            {
                rotation_x_input = Input.GetAxis("Horizontal Mouse Axis"); //Mouse Horizontal Axis
                rotation_y_input = Input.GetAxis("Vertical Mouse Axis"); //Mouse Vertical Axis

                hor_movement_input = Input.GetAxis("Horizontal Keyboard Axis");//Horizontal Movement Input
                ver_movement_input = Input.GetAxis("Vertical Keyboard Axis"); //Vertical Movement Input

                jump_input = Input.GetButtonDown("Jump Button Keyboard"); //Get Keyboard Jump Input
                crouch_input = Input.GetButtonDown("Crouch Button Keyboard"); //Get Keyboard Crouch Input
            }

        }

        //rotation_y_input = Mathf.Clamp(rotation_y_input, -45f, 45f);
        
    }

    void jump()
    {
        if (player.isGrounded == true)
        {
            vertVelocity = jumpVelocity;
            print("Player Jumped");
            hasJumped = true;
        }
    }

    private void ApplyGravity()
    {
        if (player.isGrounded == true)
            {
            if (hasJumped == false)
                {
                vertVelocity = Physics.gravity.y;
                }
            else
                {
                vertVelocity = jumpVelocity;
                }
            }
        else
            {
            vertVelocity += Physics.gravity.y * Time.deltaTime;
            hasJumped = false;
            }

    }
}
