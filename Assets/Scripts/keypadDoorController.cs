﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keypadDoorController : MonoBehaviour
{
    Animator anim;
    public GameObject Player;
    public GameObject Door;
    public float viewDistance = 5f;
    private float targetDistance = 1f;
    private bool canOpen = false;
    public GameObject InteractPanel;
    

    
    public virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, viewDistance);
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = Door.GetComponent<Animator>();
    }

    


    // Update is called once per frame
    void Update()
    {
    targetDistance = Vector3.Distance(transform.position, Player.transform.position);
     
    //Get Distance between Player and Keypad
    if (targetDistance <= viewDistance)
        {
        canOpen = true;
        }
    else
        {
        canOpen = false;
        }

    if ((Input.GetButtonDown("Interact")) && canOpen)
        {
            anim.Play("Open");
            print("Door is the opened");
        }
        /*if (canOpen)
            {
                InteractPanel.SetActive(true);

            }
            else
            {
                InteractPanel.SetActive(false);
            }*/
    }
}